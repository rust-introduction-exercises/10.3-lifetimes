use std::fmt::Display;

fn longest_with_an_announcement<'a, T>(
    x: &'a str,
    y: &'a str,
    ann: T,
) -> &'a str
where
    T: Display,
{
    println!("Announcement! {}", ann);
    if x.len() > y.len() {
        x
    } else {
        y
    }
}

fn main() {
    let res : &str;
    {
        let str_1 : &str = "fooooooo";
        let str_2 : &str = "baaaar";
        let num = 42;

        res = longest_with_an_announcement(str_1, str_2, num);
    }
    println!("Longest string is \"{}\"", res);
}
